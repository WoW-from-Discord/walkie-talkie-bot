import asyncio
import discord
from discord.ext import commands
import random
import time
import re
import datetime
import botkey
from os import listdir
from os.path import isfile, join
import sys, traceback

description = "Walkie Talkie"
bot = commands.Bot(command_prefix='!', description=description)
client = discord.Client()

@bot.command(pass_context=True)
async def say(self, ctx, channelid,*, something : str):
    await bot.send_message(discord.Object(channelid), '{}'.format(something))
    await bot.say("Sent!")
    return

@bot.event
async def on_command_error(error, ctx):
	raise error
	
@bot.event
async def on_ready():
	print('Logged in as')
	print(bot.user.name)
	print(bot.user.id)
	print('------------------------------------')
	print('Invite link for bot: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8'.format(bot.user.id))

@bot.event
async def on_message(message):
	await bot.process_commands(message)

bot.run(botkey.getToken())
